import  Vue from 'vue'

export default {

    state: {
        conversations:[],
        users:[]
    },
    getters:{
        CONVERSATIONS: state => {
            return state.conversations.sort((a, b)=>{
                if(a.createdAt < b.createdAt){
                    return  1
                }

                return -1
            });
        },
        USERS: state => state.users,
        MESSAGES: state => conversationId =>{
         return state.conversations.find(i=> i.conversationId === conversationId).messages
        },
    },
    mutations:{
        SET_CONVERSATION: (state,payload) =>{
            state.conversations = payload
        },
        SET_USERS:(state,payload)=>{state.users=payload},

        SET_MESSAGES:(state,{conversationId,payload})=>{
            Vue.set(
                state.conversations.find(i=> i.conversationId === conversationId),
                'messages',
                payload
            )
        },
        ADD_MESSAGE: (state , {conversationId,payload})=>{
            state.conversations.find(i=> i.conversationId === conversationId).messages.push(payload)
        },
        SET_CONVERSATION_LAST_MESSAGE:(state , {conversationId,payload})=>{
          let rs= state.conversations.find(i=> i.conversationId === conversationId)
            rs.content= payload.content
            rs.createdAt = payload.createdAt
        },
        UPDATE_CONVERSATIONS: (state,payload)=>{
            let rs= state.conversations.find(i=> i.conversationId === payload.conversation.id)
            rs.content= payload.content
            rs.createdAt = payload.createdAt
        }

    },
    actions:{
        GET_CONVERSATIONS:({commit})=>{
            return fetch('/conversations')
                .then(result => {
                    return result.json()
                })
                .then((result)=>{
                    commit('SET_CONVERSATION',result)
                })
        },
        ADD_CONVERSATION:({commit},userId)=>{
            let formData=new  FormData();
            formData.append("otherUser",userId)
            return fetch('/conversations/',{method:"POST",body:formData})
                .then(result => result.json())
                .then((result)=>{
                    commit('SET_CONVERSATION', result)
                })
        },
        GET_USERS:({commit})=>{
            return fetch('/contacts').then(result=>{
                return result.json()
            }).then((result)=>{
                commit('SET_USERS',result)
            })
        },
        GET_MESSAGES:({commit , getters},conversationId)=>{
            if(getters.MESSAGES(conversationId) === undefined){
                return fetch('/messages/'+conversationId)
                .then(result => result.json())
                .then((result)=>{
                    commit('SET_MESSAGES', {conversationId, payload: result})
                })
            }
        },
         POST_MESSAGE:({commit}, {conversationId, content})=>{
            let formData=new  FormData();
            formData.append("content",content)
            return fetch('/messages/'+conversationId,{method:"POST",body:formData})
                .then(result => result.json())
                .then((result)=>{
                    commit('ADD_MESSAGE', {conversationId, payload: result})
                    commit('SET_CONVERSATION_LAST_MESSAGE', {conversationId, payload: result})

                })
        },
        POST_MEDIA:({commit}, {conversationId, media})=>{
            let formData=new  FormData();
            formData.append("file",media)
            return fetch('/messages/'+conversationId,{method:"POST",body:formData})
                .then(result => result.json())
                .then((result)=>{
                    commit('ADD_MESSAGE', {conversationId, payload: result})
                    commit('SET_CONVERSATION_LAST_MESSAGE', {conversationId, payload: result})

                })
        },
        DELETE_CONVERSATION:({commit},conversationId)=>{

            return fetch('/conversations/delete/'+conversationId)
                .then(result => result.json())
        }
    }

}
