import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './components/App'
import store from "./store/store";
import Blank from "./components/Right/Blank";
import Right from "./components/Right/Right";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import express from "express";
// import  bodyParser from "express";

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
// const app = express()
//
// // Middleware
// app.use(bodyParser.urlencoded({ limit: '100mb', extended: true, parameterLimit: 100000 }))
// app.use(bodyParser.json({ limit: '100mb' }))

const routes = [
    {
        name : 'blank',
        path: '/',
        component:Blank
    },
    {
        name : 'conversation',
        path: '/conversation/:id',
        component:Right
    },
];
store.commit("SET_USERNAME",document.querySelector('#app').dataset.username);
const router =new VueRouter({
    mode: "abstract",
    routes:routes
})
new Vue({
    render: h=> h(App),
    store,
    router

}).$mount('#app')
router.replace("/")