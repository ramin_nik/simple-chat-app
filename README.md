this app is a simple chat app  for make a real time conversation between two participants and it's based on mercure and webpack encore and vue js for front end + you can send any image type! 

<b>Execution instructions

first of all you need to run this command ``composer install`` to install app bundle and libraries 

and use ``yarn install`` or ``npm install`` to install Js plugins and libraries for frontend 

this app based on docker compose, for run this you have to install docker first and go to app folder and run docker-compose up command 
and app run in ``http://localhost:888``


and remember you must make copy from ``.env.dist`` and rename it to ``.env `` this file have contains default values for the environment variables needed by the app
 

and after all above instructions run ./console.sh ``doctrine:migrations:migrate``
for make db tables , ``./console.sh`` is a small bash file for larger command itrate to php commands
if u get this its mean you doing a big job XD

<img src="https://uupload.ir/files/8kib_screenshot_۲۰۲۱۰۱۲۶_۱۷۰۷۰۲.png" alt="image"/>
