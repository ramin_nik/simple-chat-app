<?php

namespace App\Controller;

use App\Entity\Conversation;
use App\Entity\Message;
use App\Repository\MessageRepository;
use App\Repository\ParticipantRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/messages", name="message.")
 */
class MessageController extends AbstractController
{
    const ATTRIBUTES_TO_SERIALIZE = ['id', 'content', 'createdAt', 'mine','media'];
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var MessageRepository
     */
    private $messageRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ParticipantRepository
     */
    private $participantRepository;


    public function __construct(EntityManagerInterface $entityManager,
                                MessageRepository $messageRepository,
                                UserRepository $userRepository,
                                ParticipantRepository $participantRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->messageRepository = $messageRepository;
        $this->userRepository = $userRepository;
        $this->participantRepository = $participantRepository;
    }

    /**
     * @Route("/{id}", name="getMessages",methods={"GET"})
     * @param Request $request
     * @param Conversation $conversation
     * @return Response
     */
    public function getMessages(Request $request, Conversation $conversation): Response
    {
        //can i view conversation
        $this->denyAccessUnlessGranted('view', $conversation);
        $messages = $this->messageRepository->findMessageByConversationId(
            $conversation->getId()
        );
        /**
         * @var  Message $message
         */
        array_map(function ($message) {
            $message->setMine(
                $message->getUser()->getUsername()
            );
        }, $messages);
        return $this->json($messages, Response::HTTP_OK, [], [
            'attributes' => self::ATTRIBUTES_TO_SERIALIZE
        ]);
    }

    /**
     * @Route("/{id}",name="newMessage",methods={"POST"})
     * @param Request $request
     * @param SluggerInterface $slugger
     * @param PublisherInterface $publisher
     * @param Conversation $conversation
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * @throws \Exception
     */
    public function newMessage(Request $request,SluggerInterface $slugger, PublisherInterface $publisher, Conversation $conversation, SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();
        $recipient = $this->participantRepository->findParticipantByConversationIdAndUserId(
            $conversation->getId(),
            $user->getId()
        );
        $content = $request->get('content', null);

        $message = new Message();
        if($content!=null){
            $message->setContent($content);
        }
        $message->setUser($user);
        if($request->files->get('file')!=null){
            try {
                $data = $request->files->get('file');
                $originalFilename = pathinfo($data->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($user->getUsername().'-'.$originalFilename);
                $fileName=$safeFilename.'-'.uniqid().'.'.$data->guessExtension();

                $data->move(
                    $this->getParameter('media_directory'),
                    $fileName
                );
                $message->setContent("IMAGE");

                $message->setMedia( $fileName);
            }catch (\Exception $e){
                throw $e;
            }

        }
        $conversation->addMessage($message);
        $conversation->setLastMessage($message);

        $this->entityManager->getConnection()->beginTransaction();
        try {
            $this->entityManager->persist($message);
            $this->entityManager->persist($conversation);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $e) {
            $this->entityManager->rollback();
            throw $e;

        }

        $messageSerialized = $serializer->serialize($message, 'json', [
            'attributes' => ['id', 'content', 'createdAt', 'user' => ['email'], 'conversation' => ['id'],'media']
        ]);

        $update = new Update(
            [
                sprintf("/conversations/%s", $conversation->getId()),
                sprintf("/conversations/%s", $recipient->getUser()->getUsername()),
            ], $messageSerialized
        );
        $publisher($update);
        return $this->json($message, Response::HTTP_CREATED, [], [
            'attributes' => self::ATTRIBUTES_TO_SERIALIZE
        ]);
    }

    /**
     * @Route("/pushIsTyping/{id}",name="isTyping",methods={"POST"})
     * @param Request $request
     * @param PublisherInterface $publisher
     * @param Conversation $conversation
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function isTyping(Request $request, PublisherInterface $publisher, Conversation $conversation, SerializerInterface $serializer): JsonResponse
    {
        $user = $this->getUser();
//        $recipient = $this->participantRepository->findParticipantByConversationIdAndUserId(
//            $conversation->getId(),
//            $user->getId()
//        );
        $data= $request->get('isTyping',null);
        $isTyping = ['isTyping' => $data, 'username' => $user];
        $messageSerialized = $serializer->serialize($isTyping, 'json', [
            'attributes' => ['isTyping', 'username']
        ]);

        $update = new Update(
            [
                sprintf("/isTyping/%s", $conversation->getId()),
//                sprintf("/isTyping/%s", $recipient->getUser()->getUsername()),
            ], $messageSerialized
        );
        $publisher($update);
        return $this->json("done");
    }

//    /**
//     * @param SluggerInterface $slugger
//     * @return JsonResponse
//     * @throws \Exception
//     */
//    public function uploadTest(Request $request,): JsonResponse
//    {
//        $user=$this->getUser()->getUsername();
//
//
//        return $this->json('done');
//    }


}
