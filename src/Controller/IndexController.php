<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    const ATTRIBUTES_TO_SERIALIZE = ['id', 'email'];
    /**
     * @var UserRepository
     */
    private $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository=$userRepository;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $username = $this->getUser()->getUsername();
//        $token = (new Builder())
//            ->withClaim('mercure', ['subscribe' => [sprintf("/%s", $username)]])
//            ->getToken(
//                new Sha256(),
//                new Key($this->getParameter('mercure_secret_key'))
//            );
        $response = $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
        $response->headers->setCookie(new Cookie('mercureAuthorization',null,(new \DateTime())
                ->add(new \DateInterval('PT2H')),
                '/.well-known/mercure',
                null,
                false,
                true,
                false,
                'strict')
        );
        return $response;
    }
    /**
     * @Route("/contacts", name="contact",methods={"GET"})
     * @return Response
     */
    public function push(): Response
    {
        $allUsers= $this->userRepository->findAll();

        return $this->json($allUsers,Response::HTTP_OK, [], [
            'attributes' => self::ATTRIBUTES_TO_SERIALIZE
        ]);
    }
}
