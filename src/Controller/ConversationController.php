<?php

namespace App\Controller;

use App\Entity\Conversation;
use App\Entity\Participant;
use App\Repository\ConversationRepository;
use App\Repository\MessageRepository;
use App\Repository\ParticipantRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/conversations", name="conversation.")
 */
class ConversationController extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ConversationRepository
     */
    private $conversationRepository;
    /**
     * @var MessageRepository
     */
    private $messageRepository;
    /**
     * @var ParticipantRepository
     */
    private $participantRepository;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager,
                                ConversationRepository $conversationRepository,
                                MessageRepository $messageRepository,ParticipantRepository $participantRepository
    )
    {

        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->conversationRepository = $conversationRepository;
        $this->messageRepository = $messageRepository;
        $this->participantRepository = $participantRepository;
    }

    /**
     * @Route("/", name="newConversation", methods={"POST"})
     * @param Request $request
     * @param PublisherInterface $publisher
     * @return JsonResponse
     * @throws Exception
     */
    public function newConversation(Request $request,PublisherInterface $publisher): Response
    {
        $otherUser= $request->get('otherUser',0);
        $otherUser= $this->userRepository->find($otherUser);
        if(is_null($otherUser)){

            throw new Exception("user not found");
        }
        // cannot create a conversation with your self
        if($otherUser->getId()===$this->getUser()->getId()){
            throw new Exception("That's deep but cannot create a conversation with your self!");

        }
        // Check if conversation already exists
        $conversation = $this->conversationRepository->findConversationByParticipants(
          $otherUser->getid(),
          $this->getUser()->getId()
        );

        if(count($conversation)){
            return $this->json(
               "Conversation already exist!"
                ,Response::HTTP_CONFLICT);
        }
        $conversation = new Conversation();
        $participant=new Participant();
        $participant->setUser($this->getUser());
        $participant->setConversation($conversation);

        $otherParticipant=new Participant();
        $otherParticipant->setUser($otherUser);
        $otherParticipant->setConversation($conversation);

        $this->entityManager->getConnection()->beginTransaction();
        try {
            $this->entityManager->persist($conversation);
            $this->entityManager->persist($participant);
            $this->entityManager->persist($otherParticipant);

            $this->entityManager->flush();
            $this->entityManager->commit();
        }catch (\Exception $e){
            $this->entityManager->rollback();
            throw $e;
        }
        $conversation=$this->conversationRepository->findConversationByUser($this->getUser()->getId());
        $jsonData=[];
        foreach ($conversation as $conv){
            $jsonData=[$conv];
        }
        $update = new Update(
            sprintf("/newConversation/")
           , json_encode($jsonData)
        );
        $publisher($update);
        return $this->json(
            $conversation
        ,Response::HTTP_CREATED);
    }

    /**
     * @Route("/",name="getConversation",methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getConversation (Request $request){
        $conversation=$this->conversationRepository->findConversationByUser($this->getUser()->getId());
        return $this->json($conversation);

    }


    /**
     * @Route("/delete/{id}",name="deleteConversation",methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteConversation (Request $request){
        $conversationId=$request->get('id',null);
        //finding
        $conversation=$this->conversationRepository->find($conversationId);
        $messages= $this->messageRepository->findMessageByConversationId($conversationId);
//        dd($lastMessage);
        $participants=$this->participantRepository->findParticipantByConversationId($conversationId);

        try {
            //delete participants
            foreach ($participants as $participant ){
                $this->entityManager->remove($participant);
                $this->entityManager->flush();
            }
            //make them null
            $conversation->setLastMessage(null);
            $this->entityManager->flush();

            foreach ($messages as $message){
                $nullMessage= $message;
                $message->setConversation(null);
                $this->entityManager->flush();
                $this->entityManager->remove($nullMessage);
                $this->entityManager->flush();
            }

            //removing conversations
            $this->entityManager->remove($conversation);
            $this->entityManager->flush();

        }catch (Exception $e){
            return $this->json("not done".$e);

        }
        return $this->json("done");
    }
}
